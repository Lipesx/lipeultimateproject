﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class Faleconosco
    {
        [Key]
        public int IdMsg { get; set; }

        [MaxLength(50,ErrorMessage ="Digite Nome")]
        public string Nome { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Mensagem { get; set; }

    }
}