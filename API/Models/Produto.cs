﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class Produto
    {
        [Key]
        public int IdProduto { get; set; }

        public string Nome { get; set; }

        public string Tipo { get; set; }

        public string Descricao { get; set; }

        public string Img_url { get; set; }

        public decimal Preco { get; set; }


    }
}