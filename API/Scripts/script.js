﻿var app = angular.module("myModule", ['ngRoute'])
.controller("myController", function ($scope, $http) {
    $http.get('/api/Employees').then(function (response) {
        $scope.employees = response.data;
    })
});



app.controller("newController", function ($scope, $http) {
    $http.get('/api/Produtoes').then(function (response) {
        $scope.produto = response.data;
    });

});

app.controller("postcontroller", function ($scope, $http) {
    $scope.post = function (nome, email, mensagem) {
        $scope.nome = null;
        $scope.email = null;
        $scope.mensagem = null;

        var data = {
            Nome: nome,
            Email: email,
            Mensagem: mensagem
        };
        $http.post('/api/FaleConoscoes', JSON.stringify(data)).then(function (response) {
            if (response.data)
                alert("Mensagem Enviada");
        }, function (response) {
            $scope.msg = "Serviço Nao encontrado";
            $scope.statusval = response.status;
            $scope.statustext = response.statusText;
            $scope.headers = response.headers();
        });
    };

});

app.controller('postProduto', function ($scope, $http) {
    $scope.postproduto = function (nome, preco, imagen) {
        $scope.nome = null;
        $scope.preco = null;
        $scope.imagen = null;

        var data = {
            Nome: nome,
            Preco: preco,
            Img_url: imagen
        };
        $http.post('/api/Produtoes', JSON.stringify(data)).then(function (response) {
            if (response.data)
                alert("Produto Salvo");
        }, function (response) {
            $scope.msg = "Serviço Nao encontrado";
            $scope.statusval = response.status;
            $scope.statustext = response.statusText;
            $scope.headers = response.headers();
        });
    };
});

app.controller('deleteProduto', function ($scope, $http) {
    $scope.DeleteData = function (id) {
        var data = $.param({
            IdProduto: $scope.id
        });

        $http.delete('/api/Produtoes/' + $scope.id).then(function (response) {
            //$scope.ServerResponse = data;
            $scope.id = "";
            alert("Produto Deletado");
        })
        .then(function (response) {
            $scope.msg = "Serviço Nao encontrado";
            $scope.statusval = response.status;
            $scope.statustext = response.statusText;
            $scope.headers = response.headers();
        });
    };
});


app.controller('updateProduto', function ($scope, $http) {

    $scope.updateData = function () {
        var data = {
            IdProduto: $scope.id,
            Nome: $scope.nome,
            Preco: $scope.preco
        };
        $http.put('/api/Produtoes/' + $scope.id, JSON.stringify(data)).then(function (data, status, headers) {
            alert("Produto Atualizado");
        })
        .then(function (data, status, header, config) {
            console.log(erro);
        })
    }
});




    
function reload() {
    window.location.href = window.location.href
}


