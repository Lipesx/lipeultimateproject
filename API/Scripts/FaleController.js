﻿var FaleController = function ($scope,$http) {
    $http({
        url:'http://localhost:53646/api/FaleConoscoes',
        method:"POST",
        data: {
            'Nome' : $scope.nome,
            'Email':$scope.email,
            'Mensagem':$scope.mensagem
        }
    }).then(function Sucesso (response) {
        $scope.faleconosco = response.data;
    }), function Erro(response) {
        console.log(response);
    }
}

FaleController.$inject = ['$scope'];