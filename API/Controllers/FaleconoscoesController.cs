﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class FaleconoscoesController : ApiController
    {
        private APIContext db = new APIContext();

        // GET: api/Faleconoscoes
        public IQueryable<Faleconosco> GetFaleconoscoes()
        {
            return db.Faleconoscoes;
        }

        // GET: api/Faleconoscoes/5
        [ResponseType(typeof(Faleconosco))]
        public IHttpActionResult GetFaleconosco(int id)
        {
            Faleconosco faleconosco = db.Faleconoscoes.Find(id);
            if (faleconosco == null)
            {
                return NotFound();
            }

            return Ok(faleconosco);
        }

        // PUT: api/Faleconoscoes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFaleconosco(int id, Faleconosco faleconosco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != faleconosco.IdMsg)
            {
                return BadRequest();
            }

            db.Entry(faleconosco).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FaleconoscoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Faleconoscoes
        [ResponseType(typeof(Faleconosco))]
        public IHttpActionResult PostFaleconosco(Faleconosco faleconosco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Faleconoscoes.Add(faleconosco);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = faleconosco.IdMsg }, faleconosco);
        }

        // DELETE: api/Faleconoscoes/5
        [ResponseType(typeof(Faleconosco))]
        public IHttpActionResult DeleteFaleconosco(int id)
        {
            Faleconosco faleconosco = db.Faleconoscoes.Find(id);
            if (faleconosco == null)
            {
                return NotFound();
            }

            db.Faleconoscoes.Remove(faleconosco);
            db.SaveChanges();

            return Ok(faleconosco);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FaleconoscoExists(int id)
        {
            return db.Faleconoscoes.Count(e => e.IdMsg == id) > 0;
        }
    }
}